<?php
    $m = array(
        'eferole' => array(
            'quickname'  => 'Eric',
            'name'  => 'Éric Férole',
            'poste' => 'Président et stratège numérique',
            'posteTel'  => '205',
        ),
        'bbeauchemin' => array(
            'quickname'  => 'Ben',
            'name'  => 'Benoit Beauchemin',
            'poste' => 'Partenaire associé',
            //'posteTel'  => '',
        ),
        'fpilon' => array(
            'quickname'  => 'Fred',
            'name'  => 'Frédéric Pilon',
            'poste' => 'Intégrateur Web',
            //'posteTel'  => '',
        ),
        'smarquette' => array(
            'quickname'  => 'Sonia',
            'name'  => 'Sonia Marquette',
            'poste' => 'Programmeur',
            //'posteTel'  => '',
        ),
        'gbeauchemin' => array(
            'quickname'  => 'Guy',
            'name'  => 'Guy Beauchemin',
            'poste' => 'Partenaire associé',
            //'posteTel'  => '',
        ),
    )
?>
<?php if(isset($_GET['membre'])) : ?>

    <?php if($_GET['image'] == 'true') : ?>
        <meta charset="UTF-8">
        <table>
            <tr>
                <td style="vertical-align:top; text-align:center;" width="130">
                    <?php if(file_exists(__DIR__.'/'.$_GET['membre'].'.jpg')) : ?>
                        <img src="http://pixelcircusclient.com/signature_courriels/<?php echo $_GET['membre'] ?>.jpg" /><br /><br />
                    <?php else : ?>
                        <img src="http://pixelcircusclient.com/signature_courriels/default.jpg" /><br /><br />
                    <?php endif; ?>
                    <a href="https://www.linkedin.com/company-beta/1372230/" target="_blank"><img src="http://pixelcircusclient.com/signature_courriels/img/i-linkedin.png" /></a><!--
                 --><a href="https://twitter.com/pixelcircus" target="_blank"><img src="http://pixelcircusclient.com/signature_courriels/img/i-twitter.png" style="margin:0 6px;" /></a><!--
                 --><a href="https://www.facebook.com/pixelcircusmtl/" target="_blank"><img src="http://pixelcircusclient.com/signature_courriels/img/i-facebook.png" /></a>
                </td>
                <td style="vertical-align:top;" width="20">
                    &nbsp;
                </td>
                <td style="vertical-align:top;">
                    <a href="http://www.pixelcircus.ca/?utm_source=Courriel_<?php echo $m[$_GET['membre']]['quickname']; ?>&utm_medium=Courriel&utm_campaign=Signature_Courriel" target="_blank"><img src="http://pixelcircusclient.com/signature_courriels/img/logo_pixelcircus.png" /></a><br />
                    <br />
                    <!-- TO CHANGE -->
                    <strong style="font-family:Arial,sans-serif;font-size:14px;font-weight:700;color:#262626;line-height: 24px;"><?php echo $m[$_GET['membre']]['name']; ?></strong><br />
                    <span style="font-family:Arial,sans-serif;font-size:12px;color:#262626;line-height: 9px;"><?php echo $m[$_GET['membre']]['poste']; ?></span><br />
                    <a style="font-family:Arial,sans-serif;font-size:12px;font-weight:700;color:#262626;text-decoration:none;" href="tel:5145045454">514 504-5454 <?php if(isset($m[$_GET['membre']]['posteTel'])) : ?>poste <?php echo $m[$_GET['membre']]['posteTel'] ; endif; ?></a>
                    <!-- END CHANGES, OUBLIE PAS L'IMAGE DANS LA CASE D'AVANT ET LE REFERRAL DANS LES 2 LIENS -->
                    <br />
                    <br />
                    <img src="http://pixelcircusclient.com/signature_courriels/img/separator.png" /><br />
                    <br />
                    <span style="font-family:Arial,sans-serif;font-size:12px;color:#262626;">
                        5605, avenue de Gaspé, bureau 705<br />
                        Montréal, Québec  H2T 2A4
                    </span><br />
                    <a href="http://www.pixelcircus.ca/?utm_source=Courriel_<?php echo $m[$_GET['membre']]['quickname']; ?>&utm_medium=Courriel&utm_campaign=Signature_Courriel" style="font-family:Arial,sans-serif;font-size:12px;font-weight:700;color:#262626;text-decoration:none !important;" target="_blank">www.pixelcircus.ca</a><br /><br /><br />

                    <span style="font-family:Arial,sans-serif;font-size:12px;color:rgba(38,38,38,0.3)">Avis de confidentialité : Le présent message est à l’usage exclusif du ou des destinataire(s) mentionné(s) ci-dessus et peut contenir de l’information confidentielle ou privilégiée. Toute utilisation de cette information par d’autres personnes est interdite. Si vous avez reçu ce message par erreur, S.V.P., répondez à l’expéditeur et supprimez toutes copies.</span>
                </td>
            </tr>
        </table>
    <?php else : ?>
        <meta charset="UTF-8">
        <table>
            <tr>
                <td style="vertical-align:top;">
                    <a href="http://www.pixelcircus.ca/?utm_source=Courriel_<?php echo $m[$_GET['membre']]['quickname']; ?>&utm_medium=Courriel&utm_campaign=Signature_Courriel" target="_blank"><img src="http://pixelcircusclient.com/signature_courriels/img/logo_pixelcircus.png" /></a><br />
                    <br />
                    <!-- TO CHANGE -->
                    <strong style="font-family:Arial,sans-serif;font-size:14px;font-weight:700;color:#262626;line-height: 24px;"><?php echo $m[$_GET['membre']]['name']; ?></strong><br />
                    <span style="font-family:Arial,sans-serif;font-size:12px;color:#262626;line-height: 9px;"><?php echo $m[$_GET['membre']]['poste']; ?></span><br />
                    <a style="font-family:Arial,sans-serif;font-size:12px;font-weight:700;color:#262626;text-decoration:none;" href="tel:5145045454">514 504-5454 <?php if(isset($m[$_GET['membre']]['posteTel'])) : ?>poste <?php echo $m[$_GET['membre']]['posteTel'] ; endif; ?></a>
                    <!-- END CHANGES, OUBLIE PAS L'IMAGE DANS LA CASE D'AVANT ET LE REFERRAL DANS LES 2 LIENS -->
                    <br />
                    <br />
                    <img src="http://pixelcircusclient.com/signature_courriels/img/separator.png" /><br />
                    <br />
                    <span style="font-family:Arial,sans-serif;font-size:12px;color:#262626;">
                        5605, avenue de Gaspé, bureau 705<br />
                        Montréal, Québec  H2T 2A4
                    </span><br />
                    <a href="http://www.pixelcircus.ca/?utm_source=Courriel_<?php echo $m[$_GET['membre']]['quickname']; ?>&utm_medium=Courriel&utm_campaign=Signature_Courriel" style="font-family:Arial,sans-serif;font-size:12px;font-weight:700;color:#262626;text-decoration:none;" target="_blank">www.pixelcircus.ca</a><br /><br />

                    <a href="https://www.linkedin.com/company-beta/1372230/" target="_blank"><img src="http://pixelcircusclient.com/signature_courriels/img/i-linkedin.png" /></a><!--
                 --><a href="https://twitter.com/pixelcircus" target="_blank"><img src="http://pixelcircusclient.com/signature_courriels/img/i-twitter.png" style="margin:0 6px;" /></a><!--
                 --><a href="https://www.facebook.com/pixelcircusmtl/" target="_blank"><img src="http://pixelcircusclient.com/signature_courriels/img/i-facebook.png" /></a>

                    <br /><br />

                    <span style="font-family:Arial,sans-serif;font-size:12px;color:rgba(38,38,38,0.3)">Avis de confidentialité : Le présent message est à l’usage exclusif du ou des destinataire(s) mentionné(s) ci-dessus et peut contenir de l’information confidentielle ou privilégiée. Toute utilisation de cette information par d’autres personnes est interdite. Si vous avez reçu ce message par erreur, S.V.P., répondez à l’expéditeur et supprimez toutes copies.</span>
                </td>
            </tr>
        </table>
    <?php endif; ?>

<?php else : ?>
    <ul style="font-family:Arial,sans-serif;">
        <?php foreach($m as $k => $i) : ?>
            <li><a href="?membre=<?php echo $k; ?>&image=true"><?php echo $i['name']; ?> - Avec image</a></li>
            <li><a href="?membre=<?php echo $k; ?>&image=false"><?php echo $i['name']; ?> - Sans image</a></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
